﻿using System;
using System.Text;

namespace lab
{
    class MainClass
    {
        public static void Main(string[] args)
        {
			// Task 1
			bool vBool = true;
			byte vByte = 155;
			sbyte vSByte = 53;
			char vChar = '5';
			decimal vDecimal = 5.32m;//decimal range smaller then double, but precision is bigger
			double vDouble = 5.32;
			float vFloat = 5.32f;
			int vInt = -0x123;
			uint vUInt = 0x123;
			long vLong = -0x12356789;
			long vULong = 0x12356789;
			object vObject = 53;
			short vShort = -53;
			ushort vUShort = 53;    
			string vString = @"Line without \newline character";

			vInt = vByte;
			vLong = vInt;
			vLong = vULong;
			vShort = vByte;
			vUShort = vByte;
			vFloat = (float)vDouble;
			vDecimal = (decimal)vFloat;
			vInt = (int)vLong;
			vUShort = (ushort)vULong;
			vInt = (int)vFloat;

			object vObject1 = 53;
			object vObject2 = "Hello world!";
			object vObject3 = 5.32;
			vInt = (int)vObject1;
			vString = (string)vObject2;
			vDouble = (double)vObject3;

			var vVar = "Variant variable";
			Console.WriteLine(vVar);

			bool? ok = null;
			bool tester = ok ?? true;
			if (ok.HasValue)
				Console.WriteLine(ok.Value);
			else
				Console.WriteLine("is null");
			Console.WriteLine(tester);
			// Task 2
			string vString1 = "Hello";
			string vString2 = "Hello";
			string vString3 = "World";
			Console.WriteLine(vString1 == vString2);//  ==
			Console.WriteLine(vString1 == vString3);//  !=
            
			string vString4 = vString1 + " " + vString3;
			Console.WriteLine(vString4);
			string vString5 = String.Copy(vString4);
			Console.WriteLine(vString4, vString5);
			Console.WriteLine(Object.ReferenceEquals(vString4, vString5));
			Console.WriteLine(vString5.Substring(6, 5));
			string[] vString6 = "Hello my little world!".Split();
			for (int i = 0; i < vString6.Length; ++i)
				Console.Write(vString6[i] + "->");
			Console.WriteLine();
			string vString7 = vString5.Insert(6, "my hudge ");
			Console.WriteLine(vString7);
			string vString8 = vString7.Remove(6, 3);
			Console.WriteLine(vString8);
			string vNullString = null;
			string vEmptyString = String.Empty;
			//vNullString.Insert(5, "kek"); // FIXME: exception
			StringBuilder builder = new StringBuilder("Base string");
			builder[0] = 'w';
			builder.Remove(1, 3);
			builder.Insert(0, "ra");
			builder.Insert(builder.Length, " or not");
			Console.WriteLine(builder);
			// Task 3
			int[,] vMatrix = new int[10,10];
			for (int i = 0; i < vMatrix.GetLength(0); ++i)
			{
				for (int j = 0; j < vMatrix.GetLength(1); ++j)
				{
					vMatrix[i, j] = i + j;
					Console.Write(vMatrix[i,j] + " ");					
				}
				Console.WriteLine();
			}
			string[] stringArray = "Hello my little world!".Split();
			for (int i = 0; i < stringArray.Length; ++i)
				Console.WriteLine("[" + i + "]" + stringArray[i]);
			string userLine = Console.ReadLine();
			int userInt;
			if (Int32.TryParse(userLine, out userInt))
			{
				string userString = Console.ReadLine();
				if (userInt >= 0 && userInt < stringArray.Length)
				{
					stringArray[userInt] = userString;
					for (int i = 0; i < stringArray.Length; ++i)
                        Console.WriteLine("[" + i + "]" + stringArray[i]);
				}
				else
				{
					Console.WriteLine("Error: out of boudns");
				}
			}
			else
			{
				Console.WriteLine("Error: invalid input");
			}
			int[][] jaggedArray = new int[3][];
			for (int i = 0; i < jaggedArray.Length; ++i)
			{
				jaggedArray[i] = new int[i + 2];
				for (int j = 0; j < i + 1; ++j)
				{
					jaggedArray[i][j] = Int32.Parse(Console.ReadLine());
				}
			}
			for (int i = 0; i < jaggedArray.Length; ++i)
			{
				for (int j = 0; j < jaggedArray[i].Length; ++j)
				{
					Console.Write("{0} ", jaggedArray[i][j]);
				}
				Console.WriteLine();
			}
			var variantString = "Hello world!";
			var variantInt = new int[] {1, 2, 3, 4 };
			(int first, string second, char third, string fourth, ulong fifth) tuple = (1, "hello", ' ', "world", 123);
			Console.WriteLine(tuple);
			Console.WriteLine("1 - {0}, 3 - {1}, 4 - {2}", tuple.first, tuple.third, tuple.fourth);
			var (item1, item2, item3, item4, item5) = tuple;
			(int, int) tuple1 = (23, 52);
			(int, int) tuple2 = (23, 52);
			Console.WriteLine(Object.Equals(tuple1, tuple2));
            (int min, int max, char first) LocalFunction(int[] arr, string str)
			{
				int minIdx = 0, maxIdx = 0;
				for (int i = 0; i < arr.Length; ++i)
				{
					if (arr[i] > arr[maxIdx])
						maxIdx = i;
					if (arr[i] < arr[minIdx])
						minIdx = i;
				}
				return (arr[minIdx], arr[maxIdx], str[0]);
			}
			Console.WriteLine(LocalFunction(new int[] { 53, 32, 24, 31, 453, 32 }, "hello"));
        }
    }
}
