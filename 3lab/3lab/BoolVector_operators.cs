﻿using System;

namespace lab
{
	partial class BoolVector : IEquatable<BoolVector>
    {
        public override string ToString()
		{
			string src = "";
			for (int i = 0; i < data.Length; ++i)
				src += " " + data[i].ToString();
			return $"BoolVector[{data.Length}] {src}";
		}

		public bool Equals(BoolVector obj)
		{
			if (ReferenceEquals(this, obj))
				return true;
			else if (ReferenceEquals(this, null))
				return false;
			else if (ReferenceEquals(obj, null))
				return false;
			return data == obj.data;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;
			else if (ReferenceEquals(this, null))
				return false;
			else if (ReferenceEquals(obj, null))
				return false;
			return GetType() == obj.GetType() && Equals((BoolVector)obj);
		}

        public static BoolVector operator &(BoolVector vec1, BoolVector vec2)
        {
            var result = new BoolVector(Math.Min(vec1.data.Length, vec2.data.Length));
            for (int i = 0; i < result.data.Length; ++i)
                result[i] = vec1[i] & vec2[i];
            return result;
        }

        public static BoolVector operator |(BoolVector vec1, BoolVector vec2)
        {
            var result = new BoolVector(Math.Min(vec1.data.Length, vec2.data.Length));
            for (int i = 0; i < result.data.Length; ++i)
                result[i] = vec1[i] | vec2[i];
            return result;
        }

		public static BoolVector operator ~(BoolVector vec)
		{
			var result = new BoolVector(vec.data.Length);
			for (int i = 0; i < result.data.Length; ++i)
				result[i] = !vec[i];
			return result;
		}

        public bool this[int i]
        {
            get
            {
                if (checkBounds(i))
                    return data[i];
                else
                    throw new IndexOutOfRangeException("Out of bounds");
            }
            set
            {
                if (checkBounds(i))
                    data[i] = value;
                else
                    throw new IndexOutOfRangeException("Out of bounds");
            }
        }
    }
}
