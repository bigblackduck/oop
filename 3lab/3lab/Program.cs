﻿using System;

namespace lab
{
    class MainClass
    {
        public static void Main(string[] args)
        {
			var v1 = new BoolVector(12);
			var v2 = new BoolVector(12);
			var v3 = new BoolVector(new bool[] { false, true, true });
			var v4 = new BoolVector(new bool[] { true, true, false });
			Console.WriteLine(BoolVector.Information());
			Console.WriteLine(v1);
			Console.WriteLine(v3);
			Console.WriteLine($"Hashes: {v1.GetHashCode()} {v2.GetHashCode()} {v3.GetHashCode()}");
			Console.WriteLine($"AND:{v3 & v4}");
			Console.WriteLine($" OR:{v3 | v4}");
			Console.WriteLine($"NOT:{~v3}");
			Console.WriteLine($"Equality: {object.Equals(v3, v4)}");
			BoolVector[] vv = new BoolVector [10];
			for (int i = 0; i < vv.Length; ++i)
				vv[i] = new BoolVector();
			vv[3][0] = true;
			vv[6][0] = true;
			Console.WriteLine("\tWith one true");
			foreach(BoolVector item in vv)
			{
				if (item.getUnits() == 1)
					Console.WriteLine(item);
			}
			Console.WriteLine("\tWith one false");
			foreach(BoolVector item in vv)
			{
				if (item.getZeroes() == 1)
					Console.WriteLine(item);
			}
			// TODO: Равные чему?
        }
    }
}
